const knex = require("knex");
const dbConfig = require("../config/db.config");

module.exports = { db: knex(dbConfig.development) };
