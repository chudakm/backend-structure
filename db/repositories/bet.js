const { BaseRepository } = require("./base");
const { db } = require("../connection");

class BetRepository extends BaseRepository {
  constructor(db, tableName) {
    super(db, tableName);
  }

  getManyByEventIdWithoutWinner(eventId) {
    return this.db(this.tableName).where("event_id", eventId).andWhere("win", null);
  }
}

const betRepository = new BetRepository(db, "bet");
module.exports = { betRepository };
