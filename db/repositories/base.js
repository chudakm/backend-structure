class BaseRepository {
  constructor(db, tableName) {
    this.db = db;
    this.tableName = tableName;
  }

  async create(data) {
    const [result] = await this.db(this.tableName).insert(data).returning("*");
    return result;
  }

  async getAll() {
    return this.db.select().table(this.tableName);
  }

  async getById(id) {
    const [result] = await this.db(this.tableName).where("id", id).returning("*");
    return result;
  }

  async updateById(id, data) {
    const [result] = await this.db(this.tableName).where("id", id).update(data).returning("*");
    return result;
  }

  async count() {
    const [result] = await this.db(this.tableName).count("id");
    return +(result?.count || 0);
  }
}

module.exports = { BaseRepository };
