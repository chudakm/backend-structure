const { BaseRepository } = require("./base");
const { db } = require("../connection");

class OddsRepository extends BaseRepository {
  constructor(db, tableName) {
    super(db, tableName);
  }
}

const oddsRepository = new OddsRepository(db, "odds");
module.exports = { oddsRepository };
