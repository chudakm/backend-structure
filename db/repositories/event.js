const { BaseRepository } = require("./base");
const { db } = require("../connection");

class EventRepository extends BaseRepository {
  constructor(db, tableName) {
    super(db, tableName);
  }
}

const eventRepository = new EventRepository(db, "event");
module.exports = { eventRepository };
