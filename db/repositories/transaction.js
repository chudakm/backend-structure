const { BaseRepository } = require("./base");
const { db } = require("../connection");

class TransactionRepository extends BaseRepository {
  constructor(db, tableName) {
    super(db, tableName);
  }
}

const transactionRepository = new TransactionRepository(db, "transaction");
module.exports = { transactionRepository };
