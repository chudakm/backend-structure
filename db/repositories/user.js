const { BaseRepository } = require("./base");
const { db } = require("../connection");

class UserRepository extends BaseRepository {
  constructor(db, tableName) {
    super(db, tableName);
  }

  async updateBalanceById(id, balance) {
    await this.db(this.tableName).where("id", id).update("balance", balance);
  }
}

const userRepository = new UserRepository(db, "user");
module.exports = { userRepository };
