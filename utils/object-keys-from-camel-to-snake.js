const { camelToSnake } = require("./camel-to-snake");

const mapObjectKeysFromCamelToSnake = object => {
  for (const key in object) {
    const keyInSnake = camelToSnake(key);
    if (keyInSnake !== key) {
      object[keyInSnake] = object[key];
      delete object[key];
    }
  }
};

module.exports = { mapObjectKeysFromCamelToSnake };
