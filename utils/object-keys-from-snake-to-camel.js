const { snakeToCamel } = require("./snake-to-camel");

const mapObjectKeysFromSnakeToCamel = object => {
  for (const key in object) {
    const keyInCamel = snakeToCamel(key);
    if (keyInCamel !== key) {
      object[keyInCamel] = object[key];
      delete object[key];
    }
  }
};

module.exports = { mapObjectKeysFromSnakeToCamel };
