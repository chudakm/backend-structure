const jwt = require("jsonwebtoken");
const NotAuthorizedError = require("../../errors/not-authorized");

const authorize = role => async (req, _, next) => {
  const authHeader = req.headers["authorization"];
  if (!authHeader) {
    return next(new NotAuthorizedError());
  }

  const token = authHeader.replace("Bearer ", "");

  try {
    const payload = await new Promise((resolve, reject) => {
      jwt.verify(token, process.env.JWT_SECRET, (err, payload) => {
        if (err) reject(err);
        else resolve(payload);
      });
    });

    if (role && payload.type !== role) {
      throw new Error();
    }

    req.user = payload;

    next();
  } catch (err) {
    return next(new NotAuthorizedError());
  }
};

module.exports = authorize;
