const handleErrors = (err, req, res, next) => {
  if (res.headersSent) {
    return next(err);
  }

  res.status(err.status).send({ error: err.message });
};

module.exports = handleErrors;
