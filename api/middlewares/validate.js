const ValidationError = require("../../errors/validation");

const validate = (property, schema) => {
  return (req, _, next) => {
    const isValidResult = schema.validate(req[property]);

    if (isValidResult.error) {
      const error = new ValidationError(isValidResult.error.details[0].message);
      next(error);
    } else {
      next();
    }
  };
};

module.exports = validate;
