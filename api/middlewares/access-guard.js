const NotAuthorizedError = require("../../errors/not-authorized");

module.exports = (req, _, next) => {
  if (req.params.id !== req.user.id) {
    const error = new NotAuthorizedError("UserId mismatch");
    next(error);
  } else {
    next();
  }
};
