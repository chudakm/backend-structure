const { Router } = require("express");
const validationSchemas = require("../validation-schemas");
const validate = require("../middlewares/validate");
const authorize = require("../middlewares/authorize");
const betService = require("../services/bet");

const router = Router();

router.post("/", validate("body", validationSchemas.bet.create), authorize(), async (req, res, next) => {
  try {
    const response = await betService.create(req.user.id, req.body);

    return res.send(response);
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
