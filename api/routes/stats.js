const { Router } = require("express");
const authorize = require("../middlewares/authorize");
const StatsService = require("../services/stats");
const InternalServerError = require("../../errors/internal-server-error");

const router = Router();

router.get("/", authorize("admin"), async (_, res) => {
  try {
    const stats = await StatsService.get();
    res.send(stats);
  } catch (err) {
    const error = new InternalServerError();
    return next(error);
  }
});

module.exports = router;
