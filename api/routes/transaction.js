const { Router } = require("express");
const validationSchemas = require("../validation-schemas");
const validate = require("../middlewares/validate");
const authorize = require("../middlewares/authorize");
const transactionService = require("../services/transaction");

const router = Router();

router.post("/", validate("body", validationSchemas.transaction.create), authorize("admin"), async (req, res, next) => {
  try {
    const response = await transactionService.create(req.body);
    return res.send(response);
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
