const healthRoutes = require("./health");
const userRoutes = require("./user");
const transactionRoutes = require("./transaction");
const eventRoutes = require("./event");
const betRoutes = require("./bet");
const statsRoutes = require("./stats");

module.exports = app => {
  app.use("/health", healthRoutes);
  app.use("/users", userRoutes);
  app.use("/transactions", transactionRoutes);
  app.use("/events", eventRoutes);
  app.use("/bets", betRoutes);
  app.use("/stats", statsRoutes);
};
