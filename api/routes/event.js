const { Router } = require("express");
const validationSchemas = require("../validation-schemas");
const validate = require("../middlewares/validate");
const authorize = require("../middlewares/authorize");
const eventService = require("../services/event");

const InternalServerError = require("../../errors/internal-server-error");

const router = Router();

router.post("/", validate("body", validationSchemas.events.create), authorize("admin"), async (req, res, next) => {
  try {
    const response = await eventService.create(req.body);

    return res.send(response);
  } catch (err) {
    return next(new InternalServerError());
  }
});

router.put("/:id", validate("body", validationSchemas.events.update), authorize("admin"), async (req, res, next) => {
  try {
    const response = await eventService.updateById(req.params.id, req.body);
    res.send(response);
  } catch (err) {
    return next(new InternalServerError());
  }
});

module.exports = router;
