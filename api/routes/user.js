const { Router } = require("express");
const validationSchemas = require("../validation-schemas");
const validate = require("../middlewares/validate");
const authorize = require("../middlewares/authorize");
const accessGuard = require("../middlewares/access-guard");
const userService = require("../services/user");

const jwt = require("jsonwebtoken");

const router = Router();

router.get("/:id", validate("params", validationSchemas.common.id), async (req, res, next) => {
  try {
    const response = await userService.getById(req.params.id);

    return res.send(response);
  } catch (err) {
    return next(err);
  }
});

router.post("/", validate("body", validationSchemas.user.create), async (req, res, next) => {
  try {
    const user = await userService.create(req.body);

    return res.send({
      ...user,
      accessToken: jwt.sign({ id: user.id, type: user.type }, process.env.JWT_SECRET),
    });
  } catch (err) {
    return next(err);
  }
});

router.put("/:id", validate("body", validationSchemas.user.update), authorize(), accessGuard, async (req, res, next) => {
  try {
    const response = await userService.updateById(req.params.id, req.body);
    return res.send(response);
  } catch (err) {
    return next(err);
  }
});

module.exports = router;
