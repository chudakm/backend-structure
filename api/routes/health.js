const { Router } = require("express");

const router = Router();

router.get("/", (_, res) => {
  res.send("Hello World!");
});

module.exports = router;
