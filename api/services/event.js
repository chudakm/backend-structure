const { eventRepository } = require("../../db/repositories/event");
const { oddsRepository } = require("../../db/repositories/odds");
const { betRepository } = require("../../db/repositories/bet");
const { userRepository } = require("../../db/repositories/user");
const utils = require("../../utils");

class EventService {
  constructor(eventRepository, oddsRepository, betRepository, userRepository) {
    this.eventRepository = eventRepository;
    this.oddsRepository = oddsRepository;
    this.betRepository = betRepository;
    this.userRepository = userRepository;
  }

  async create(data) {
    utils.mapObjectKeysFromCamelToSnake(data.odds);

    const odds = await this.oddsRepository.create(data.odds);
    delete data.odds;

    utils.mapObjectKeysFromCamelToSnake(data);

    const event = await this.eventRepository.create({ ...data, odds_id: odds.id });

    utils.mapObjectKeysFromSnakeToCamel(event);
    utils.mapObjectKeysFromSnakeToCamel(odds);

    return { ...event, odds };
  }

  async updateById(id, data) {
    const bets = await this.betRepository.getManyByEventIdWithoutWinner(id);
    const [w1, w2] = data.score.split(":");
    let result;

    if (+w1 > +w2) {
      result = "w1";
    } else if (+w2 > +w1) {
      result = "w2";
    } else {
      result = "x";
    }

    const event = await this.eventRepository.updateById(id, { score: data.score });

    await Promise.all(
      bets.map(async bet => {
        if (bet.prediction == result) {
          await this.betRepository.updateById(bet.id, { win: true });

          // TODO: mb update balance in one query
          const user = await this.userRepository.getById(bet.user_id);
          return this.userRepository.updateById(bet.user_id, { balance: user.balance + bet.bet_amount * bet.multiplier });
        } else {
          return this.betRepository.updateById(bet.id, { win: false });
        }
      }),
    );

    utils.mapObjectKeysFromSnakeToCamel(event);

    return event;
  }
}

module.exports = new EventService(eventRepository, oddsRepository, betRepository, userRepository);
