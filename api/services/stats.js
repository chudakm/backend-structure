const { betRepository } = require("../../db/repositories/bet");
const { eventRepository } = require("../../db/repositories/event");
const { userRepository } = require("../../db/repositories/user");

class StatsService {
  constructor(userRepository, betRepository, eventRepository) {
    this.userRepository = userRepository;
    this.betRepository = betRepository;
    this.eventRepository = eventRepository;
  }

  async get() {
    const totalUsersPromise = this.userRepository.count();
    const totalBetsPromise = this.betRepository.count();
    const totalEventsPromise = this.eventRepository.count();

    const [totalUsers, totalBets, totalEvents] = await Promise.all([totalUsersPromise, totalBetsPromise, totalEventsPromise]);

    return { totalUsers, totalBets, totalEvents };
  }
}

module.exports = new StatsService(userRepository, betRepository, eventRepository);
