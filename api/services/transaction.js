const { transactionRepository } = require("../../db/repositories/transaction");
const { userRepository } = require("../../db/repositories/user");
const BadRequestError = require("../../errors/bad-request");
const utils = require("../../utils");

class TransactionService {
  constructor(transactionRepository, userRepository) {
    this.transactionRepository = transactionRepository;
    this.userRepository = userRepository;
  }

  async create(data) {
    const user = await this.userRepository.getById(data.userId);
    if (!user) throw new BadRequestError("User does not exist");

    utils.mapObjectKeysFromCamelToSnake(data);

    const transaction = await this.transactionRepository.create(data);
    const currentBalance = data.amount + user.balance;

    await this.userRepository.updateBalanceById(user.id, currentBalance);

    utils.mapObjectKeysFromSnakeToCamel(transaction);

    return { ...transaction, currentBalance };
  }
}

module.exports = new TransactionService(transactionRepository, userRepository);
