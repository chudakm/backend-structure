const { betRepository } = require("../../db/repositories/bet");
const { userRepository } = require("../../db/repositories/user");
const { eventRepository } = require("../../db/repositories/event");
const { oddsRepository } = require("../../db/repositories/odds");

const BadRequestError = require("../../errors/bad-request");
const NotFoundError = require("../../errors/not-found");

const utils = require("../../utils");

class BetService {
  constructor(betRepository, userRepository, eventRepository, oddsRepository) {
    this.betRepository = betRepository;
    this.userRepository = userRepository;
    this.eventRepository = eventRepository;
    this.oddsRepository = oddsRepository;
  }

  async create(userId, data) {
    utils.mapObjectKeysFromCamelToSnake(data);

    const user = await this.userRepository.getById(userId);
    if (!user) throw new BadRequestError("User does not exist");

    if (+user.balance < +data.bet_amount) throw new BadRequestError("Not enough balance");

    const event = await this.eventRepository.getById(data.event_id);
    if (!event) throw new NotFoundError("event");

    const odds = await this.oddsRepository.getById(event.odds_id);
    if (!odds) throw new NotFoundError("odds");

    let multiplier;
    switch (data.prediction) {
      case "w1":
        multiplier = odds.home_win;
        break;
      case "w2":
        multiplier = odds.away_win;
        break;
      case "x":
        multiplier = odds.draw;
        break;
    }

    const bet = await this.betRepository.create({ ...data, multiplier, event_id: event.id, user_id: userId });

    const currentBalance = user.balance - data.bet_amount;
    await this.userRepository.updateBalanceById(userId, currentBalance);

    utils.mapObjectKeysFromSnakeToCamel(bet);

    return { ...bet, currentBalance };
  }
}

module.exports = new BetService(betRepository, userRepository, eventRepository, oddsRepository);
