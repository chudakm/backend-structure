const { userRepository } = require("../../db/repositories/user");
const NotFoundError = require("../../errors/not-found");
const DuplicateUniqueKeyError = require("../../errors/duplicate-unique-key");
const InternalServerError = require("../../errors/internal-server-error");
const utils = require("../../utils");

class UserService {
  constructor(userRepository) {
    this.userRepository = userRepository;
  }

  async create(data) {
    try {
      const user = await userRepository.create({ ...data, balance: 0 });

      utils.mapObjectKeysFromSnakeToCamel(user);

      return user;
    } catch (err) {
      if (err.code == "23505") throw new DuplicateUniqueKeyError(err.detail);
      else throw new InternalServerError();
    }
  }

  async getById(id) {
    const user = await userRepository.getById(id);
    if (!user) {
      throw new NotFoundError("user");
    }

    return user;
  }

  async updateById(id, data) {
    try {
      return await this.userRepository.updateById(id, data);
    } catch (err) {
      if (err.code == "23505") throw new DuplicateUniqueKeyError(err.detail);
      else throw new InternalServerError();
    }
  }
}

module.exports = new UserService(userRepository);
