const joi = require("joi");

const id = joi
  .object({
    id: joi.string().uuid(),
  })
  .required();

module.exports = { id };
