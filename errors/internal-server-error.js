const BaseError = require("./base");

class InternalServerError extends BaseError {
  constructor() {
    super("Internal Server Error", 500);
  }
}

module.exports = InternalServerError;
