const BaseError = require("./base");
const utils = require("../utils");

class NotFoundError extends BaseError {
  constructor(entity) {
    const entityCapitalized = utils.capitalizeFirstLetter(entity);
    super(`${entityCapitalized} not found`, 404);
  }
}

module.exports = NotFoundError;
