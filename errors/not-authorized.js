const BaseError = require("./base");

class NotAuthorizedError extends BaseError {
  constructor(message = "Not Authorized") {
    super(message, 401);
  }
}

module.exports = NotAuthorizedError;
