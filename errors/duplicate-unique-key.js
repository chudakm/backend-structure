const BaseError = require("./base");

class DuplicateUniqueKeyError extends BaseError {
  constructor(detail) {
    super(detail, 400);
  }
}

module.exports = DuplicateUniqueKeyError;
