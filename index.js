const express = require("express");
const { db } = require("./db/connection");
const routes = require("./api/routes");
const handleErrors = require("./api/middlewares/handleErrors");

const app = express();
const port = process.env.PORT || 3000;

app.use(express.json());
app.use((uselessRequest, uselessResponse, neededNext) => {
  db.raw("select 1+1 as result")
    .then(function () {
      neededNext();
    })
    .catch(() => {
      throw new Error("No db connection");
    });
});

routes(app);

app.use(handleErrors);

const server = app.listen(port, () => {
  console.log(`App listening at http://localhost:${port}`);
});

process.on("SIGTERM", () => {
  server.close(() => {
    db.destroy(() => process.exit(0));
  });
});

// Do not change this line
module.exports = { app };
